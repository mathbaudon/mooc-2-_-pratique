Comment aller plus loin ou accéder à des ressources qui nous offrent une vision complémentaire ?

[TOC]

En voici quelques unes à discuter et partager ensemble:

### Trois tutoriels sur mixité et numérique

Trois tutoriels réalisés par [Isabelle Collet](https://fr.wikipedia.org/wiki/Isabelle_Collet) pour les collègues enseignant·e·s ou les personnes intervenantes en classe. Ils permettent de donner des conseils pour transmettre un numérique sans biais de genre et d’identifier les principaux écueils à éviter:

- [Animer un atelier mixte](https://tube-education.beta.education.fr/videos/watch/ea3ab613-
2b7f-45e3-b012-ba714451c609) parce que filles et garçons ne s’expriment pas de la même manière.
- [Représenter un rôle modèle](https://tube-education.beta.education.fr/videos/watch/7bfa3d47-
30f5-4b1d-9da6-858ac3cd5ad2) parce que nombreuses/nombreux rôles modèles transmettent inconsciemment des biais de genre.
- [Favoriser l’intérêt des filles](https://tube-education.beta.education.fr/videos/watch/bdf1cd9f-
294c-4938-8951-2f93b9f83f8b) sur des sujets pour lesquels elles viennent avec un a priori.

Ces vidéos sont libres d’usage pour servir l‘ensemble de la communauté éducative et ses partenaires. Leur utilisation peut s’imaginer, pendant et hors temps scolaire, dans le cadre de l’enseignement du numérique ou encore d’ateliers liés au numérique, de la découverte des métiers, d’interventions de rôles modèles, de médiation et de sciences du numérique.

### Mixité et numérique

Une conférence plénière d'[Isabelle Collet](https://fr.wikipedia.org/wiki/Isabelle_Collet) qui y développe sa pensée relativement à la pedagogie de l'égalité. Elle est informaticienne scientifique de formation. Elle est maintenant Professeure en sciences de l’éducation à l’Université́ de Genève. Elle a publié́ en 2019 « [Les oubliées du numérique](https://www.franceculture.fr/oeuvre/les-oubliees-du-numerique) » aux éditions Le Passeur. Elle est administratrice de la Fondation Femmes@numérique et fut Vice-présidente du Conseil d’administration de l’INSA de Lyon.

[![Video de l'intervention](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/3_Prendre-du-recul-au-niveau-didactique/3.3_Pédagogie-de-l-égalité//Ressources/femmes-et-numerique.png)](https://scolawebtv.crdp-versailles.fr/?iframe&id=60430)

[![Diaporama de l'intervention](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/3_Prendre-du-recul-au-niveau-didactique/3.3_Pédagogie-de-l-égalité//Ressources/femmes-et-numerique-collet.png)](https://www.dane.ac-versailles.fr/IMG/pdf/femmes-et-numerique-collet.pdf)

Une ressource mise en partage par l'[académie de Versailles](https://www.dane.ac-versailles.fr/etre-accompagne-se-former/femmes-et-numerique).

### Pour aller plus loin : les tables rondes sur la pédagogie de l'égalité

Ressources issues du Plan National de Formaton à l'égalité filles-garçons: pour la mixité dans les formations et les métiers du numérique, du 11 mai 1u 17 juin 2021.

#### Contexte

Plusieurs études montrent un recul de la mixité dans les filières et les métiers du numérique, tout récemment  Gender Scan dans l’innovation en 2019 avec une spécificité française : une chute de la proportion de diplômées dans le numérique (-2 % en France contre + 20 % en UE) ; baisse de la proportion de femmes actives dans les emplois de la haute technologie, progression ralentie de la mixité parmi les spécialistes en TIC (supérieure à 20 % en Europe contre 12 % en France).

Ce sont davantage des inégalités d'orientation que de réussite qui engendrent des inégalités de carrière entre les sexes. En effet, à l’école et au collège, filles et garçons ont des résultats identiques en mathématiques et dans les matières scientifiques et technologiques. Dans le numérique, c’est plutôt même à l’avantage des filles, selon une enquête internationale à laquelle la France a participé pour la première fois en 2018 (ICILS international computer and information Litteracy Study).  En 4ème les filles réussissent bien mieux en littéracie numérique et sont à égal niveau en pensée informatique.  C’est plus tard au cours de la scolarité, au moment de faire des choix de filières, que les différences apparaissent. Au lycée les filles représentaient, jusqu’en 2018, moins de 10 % des élèves des spécialités liées à l’informatique. En 2020 2,9 % d’entre elles (contre 16,8 % les garçons) ont choisi la nouvelle spécialité numérique et informatique offerte en classe de Première.

Plusieurs raisons expliquent cet état de fait : tout particulièrement le déficit de culture numérique en général, la très grande méconnaissance des métiers qui souffrent d’une image « technique » peu attractive et la persistance de stéréotypes et biais de genre qui au fur et à mesure écartent les filles de ces filières. Les métiers du numérique sont au moins au nombre de 80 et quand on interroge les élèves, ils ne connaissent que celui « d’informaticien ». A fortiori la transformation des métiers avec le numérique, ceux de la médecine, du droit, de l’agronomie ou de l’environnement, secteurs particulièrement féminisés, est ignorée. 

Constat aujourd’hui : ce déséquilibre a des conséquences économiques et sociétales. L’enjeu de féminiser ces métiers est réellement un enjeu de démocratie et de citoyenneté. 

Le ministère de l'Éducation Nationale et de la jeunesse et des sports, affiche une politique ambitieuse construite en interministériel, avec une mise en synergie de partenaires-clés instutionnels et associatifs : la DNE, la DGESCO, la Fondation femmes@numérique, l’ONISEP, Canopé, Inria, les entreprises (prioritairement celles fédérées par le CIGREF, l’AFINEF, EdtechFrance), un collectif d’associations. Les actions communes doivent sont abordées de façon transversale à l’ensemble des actions, afin d’avoir un réel impact sur l’augmentation du nombre de filles et de femmes dans les métiers du numérique. Les actions conduites doivent s’appuyer sur les résultats de la recherche et leur impact évalué en continu car de l’avis de tous les professionnels, celles qui ont été conduites jusque-là ont plutôt peu d’effet. 

#### Table ronde 1. Pourquoi la mixité dans le numérique est-elle un enjeu de société ?

- Animation/Intervention : Jean-Marie Chesneaux, IGESR
- _Le constat._ Claudine Schmuck Global Contact étude Gender Scan 
- _Pourquoi est-ce un enjeu de société ? Pourquoi en est-on arrivé là ?_ Muriel Brunet, Cheffe de projet Filles et numérique, DNE et Henri d’Agrain, Délégué général du Cigref 
- _Quel est le rôle de l’Education nationale ?_ Claude Roiron, déléguée ministérielle à l’égalité filles garçons
- _Exemple : Parallaxe 2050 (un container escape game)_ Xavier Cheney, Directeur Opérationnel du Campus des métiers et des qualifications Drômes Ardèche
  - [La vidéo.](https://files.inria.fr/LearningLab_public/C045TV/ITW/EFG_Num_TR1.mp4)

#### Table ronde 2. Comment enseigner un numérique égalitaire ? 

- Animation/Intervention : Sylvie Kocik, Déléguée régionale académique au numérique Région Nouvelle Aquitaine
- _Comment enseigner et transmettre un numérique égalitaire éclairé par la Recherche ?_ Isabelle Collet, Professeure en science de l’éducation à l’Université de Genève
- _Inventer des pratiques pédagogiques innovantes._ Sophie Viger, Directrice de 42, ex-directrice de la WebAcadémy 
- _Former les enseignants aux pratiques pédagogiques innovantes._ Marie-Caroline Missir, Directrice générale de Canopé
- _Comment évaluer l’impact des actions_. Dorothée Roch
- _Exemple : Livret tous.tes numérique_. Soledad Tolosa, Les Petits Débrouillards et Alter Egaux 
- _Exemple : Les Intrépides de la Tech_. Lucie Jagu, Fondation SIMPLON ou Claude Terrosier Magic Makers
  - [La vidéo.](https://files.inria.fr/LearningLab_public/C045TV/ITW/EFG_Num_TR2.mp4)

#### Table ronde 2. Les femmes dans les métiers du numérique

- Animation : Sabrina Caliaros Déléguée académique au numérique de l’académie de Montpellier 
- _L’orientation en question_. Frédérique Alexandre-Bally, Directrice Générale de l’Onisep et Philippe Lebreton, Chef du bureau l'orientation et de la lutte contre le décrochage scolaire
- _La mobilisation des entreprises et des réseaux_. Consuelo Benicourt, Directrice RSE Sopra Stéria et Muriel Brunet, Cheffe de projet Filles et numérique, DNE 
- _Exemple : La boite à métiers du numérique … et du genre_. Anaïs Moressa, Science animation
  - [La vidéo.](https://files.inria.fr/LearningLab_public/C045TV/ITW/EFG_Num_TR3.mp4)

### La boite à métiers de la filière du numérique

Une approche ludique pour prendre connaissance des métiers du numérique avec une prise en main autonome par les enseignants et une animation pour mettre en évidence et partager les risques sur les problématiques de genre dans ce secteur
- Cibles : animation en classe de 3eme et Lycée
- Projet accompagné par Académie (DRAIO) et DAN Montpellier
- Contacts : Anais Moressa anais.scienceanim@gmail.com
https://www.science-animation.org/fr/boite-metiers-numerique-femmes-et-numerique-kit-danimation 

Note : il y a un problème de certificat mais on peut passer outre les alertes du navigateur

### Le rapport ministériel sur « Faire de l’égalité filles-garçons une nouvelle étape dans la mise en œuvre du lycée du XXIe siècle » 

Ce [rapport du 9 Juillet 2021](https://www.education.gouv.fr/sites/default/files/2021-09/t-l-charger-le-rapport-faire-de-l-galit-filles-gar-ons-une-nouvelle-tape-dans-la-mise-en-oeuvre-du-lyc-e-du-xxie-si-cle--94424.pdf) explicite un cadre réglementaire engageant et induisant de nombreuses initiatives, mais des des constats contrastés et paradoxaux en matière d’égalité filles-garçons, comme si toutes ces mesures n'étaient que peu efficaces et la solution devaint être trouvée ailleurs.

Dans l’ensemble, les filles font toujours plus le choix d’enseignements littéraires et artistiques tandis que les garçons se tournent majoritairement vers les enseignements scientifiques. En 1re, la représentation des filles sont quasi équilibrées avec
les garçons en mathématiques (de l’ordre de 50%) et sous représentées en NSI (18%) et SI (15,4%). En terminale, elles représentent 41,8% en mathématiques (soit une baisse de 9 points par rapport à la 1re), 13% en NSI (soit une baisse de 5 points) un chiffre stable mais faible en SI. La proportion des filles et des garçons dans les doublettes de terminale prolonge et accentue les inégalités des choix de spécialités en 1re. En résumé, les choix genrés persistent, la réforme du lycée mise en place sur une seule promotion n’a pas changé les inégalités de répartition, mais ne les a pas aggravées. Il existe quelques signes d’évolution que le temps permettra de vérifier. Cette asymétrie est inverse dans les matières dites "littéraires", avec peu de garçons.



### Autres projets menés avec la fondation Femmes@Numérique en collaboration avec des Académies 

La fondation [Femmes@Numérique](https://femmes-numerique.fr) partage d'[autres projets](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/3_Prendre-du-recul-au-niveau-didactique/3.3_Pédagogie-de-l-égalité//Ressources/synthese_femmes_numerique_ressources_filles_et_numerique.pdf) pas directement liés à l'informatique au lycée, mais assez inspirants.

### Trois praticiennes parlent de leurs métiers du numérique

Les « jeudis de la recherche » de l'académie de Versailles permettent à tous les acteurs de l’éducation intéressés par un domaine de le découvrir en faisant la rencontre d’un chercheur spécialiste du domaine abordé puis en faisant celle d’un ou de plusieurs praticiens avec un témoignage concret sous l’angle des pratiques numériques.

Ici avec [Femmes et Numérique](https://www.dane.ac-versailles.fr/etre-accompagne-se-former/article/femmes-et-numerique-2-2) trois praticiennes parlent de leurs métiers du numérique.

### Femmes et Sciences : et si c’était une affaire de mecs ?

Un [article de médiation scientifique](https://www.lemonde.fr/blog/binaire/2019/12/19/femmes-et-sciences-et-si-cetait-une-affaire-de-mecs/) un peu décalé ``écrit´´ par un petit garçon de 6 mois présente sur le blong Binaire du Monde.fr, à travers les contributions de deux grandes scientifiques de la pédagogie de l'égalité [Clémence Perronnet](https://www.cnrs.fr/fr/personne/clemence-perronnet) et  [Isabelle Collet](https://fr.wikipedia.org/wiki/Isabelle_Collet) cette problématique de la pédagogie de l'égalité. Cet article collectif est issu d'une intervention au congrès de l'association [Femmes et Sciences](https://www.femmesetsciences.fr/).
