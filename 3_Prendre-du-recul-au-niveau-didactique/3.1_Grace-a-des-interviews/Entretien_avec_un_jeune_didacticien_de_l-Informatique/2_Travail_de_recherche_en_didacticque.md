# Didactique de l'informatique : quelques éléments d'introduction par Olivier Goletti

Olivier Goletti est un jeune chercheur en didactique de l'informatique, doctorant à l'UCLouvain. 
Il présente ses travaux de recherche en cours, nous parle des bases théoriques des sciences de l'éducation et des 4 stratégies expérimentées par des tuteurs qui encadrent le premier cours d'informatique de la faculté polytechnique, l'[EPL](https://uclouvain.be/fr/facultes/epl).

[![Didactique de l'informatique : quelques éléments d'introduction par Olivier Goletti](https://mooc-nsi-snt.gitlab.io/portail/assets/diapo-didactique1.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-Didactique-OG.mp4)



