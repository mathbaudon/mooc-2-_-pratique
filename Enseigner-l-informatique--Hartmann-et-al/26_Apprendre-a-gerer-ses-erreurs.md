---
title: 26. Apprendre à gérer ses erreurs
---

# Apprendre à gérer ses erreurs


Dans le cadre d'un travail en commun sur un volumineux rapport, les
étudiants utilisent un système de contrôle de version. Cela ne se
déroule néanmoins pas sans heurts : un étudiant se plaint parce qu'il a
ajouté un nouveau document et que celui-ci reste invisible aux autres.
L'enseignante examine la structure des dossiers sur l'ordinateur de
l'étudiant et, au bout d'un certain temps, lui demande s'il a renommé
des dossiers ou des fichiers. L'étudiant confirme et, quelques clics
plus tard, l'enseignante annonce que tout fonctionne à présent de
nouveau. Elle a pu s'appuyer sur sa propre expérience dans le dépannage
et sur ses connaissances fondamentales du fonctionnement du système de
contrôle de version pour isoler puis éliminer l'erreur. L'étudiant peut
continuer à travailler, mais il reste un sentiment de malaise : comment
le professeur d'informatique a-t-elle su quoi faire ? Que devrai-je
faire au prochain problème ?

---

**Problème :** les problèmes sont récurrents lors des travaux sur
ordinateur. La résolution de ces problèmes prend du temps et perturbe le
déroulement du cours. L'enseignant est tenté de résoudre lui-même les
problèmes le plus rapidement possible et de manquer ainsi une bonne
occasion de communiquer aux élèves d'utiles stratégies de recherche des
erreurs.


La recherche des erreurs (ou débogage) est l'une des activités
essentielles du développement de logiciels. Les tests et le débogage
font partie intégrante de tout développement logiciel et les estimations
actuelles donnent les fréquences d'erreurs suivantes pour 1000 lignes de
code :


Logiciels normaux 25 erreurs  
Logiciels importants 2-3 erreurs  
Logiciels médicaux 0,2 erreur  
Logiciels de navette spatiale *<* 0,1 erreur


Les implications de ces estimations sont considérables : un système
d'exploitation moderne compte environ 50 millions de lignes de code, ce
qui donne un total de 50 000 erreurs même en supposant un taux réduit de
l'ordre de 1 erreur pour 1000 lignes de code. Ces chiffres sont bien
évidemment à interpréter avec prudence, même s'ils sont étayés par des
études empiriques (par ex. [CMKC03]).


Les erreurs surviennent toujours au moment le plus inattendu pendant une
tâche sur ordinateur. La question de savoir comment gérer ces erreurs
est typique de l'enseignement de l'informatique. Dans les autres
matières, les exercices et les expériences se déroulent souvent dans un
environnement spécifique adapté à la formation. Les cours de physique ou
de chimie, par exemple, ont lieu dans un laboratoire spécialement conçu
à cet effet. En informatique, par contre, les élèves qui réalisent des
exercices pratiques sur ordinateur sont directement confrontés au monde
réel. Ils utilisent Windows ou MacOS et non un système d'exploitation
simplifié. Leur traitement de texte Word dispose de toutes ces
fonctionnalités, il ne s'agit pas d'une version allégée pour l'école.


Les causes des erreurs lors d'un travail sur ordinateur sont multiples :
les utilisateurs font des fautes d'inattention, chaque nouvelle version
d'un programme contient des modifications, un même logiciel se
comportera différemment suivant le système d'exploitation, les
programmes ne sont pas toujours conviviaux et intuitifs, un trop grand
nombre de fonctions entraîne une saturation de l'utilisateur, chaque
logiciel contient des erreurs de programmation, etc. Il peut même
parfois arriver que le matériel soit défectueux, ce qui se manifeste par
des erreurs indirectes et rend la cause véritable plus difficile à
identifier. Les messages d'erreur des logiciels sont fréquemment
incompréhensibles et laissent l'utilisateur perplexe. Les connaissances
préalables des élèves sont insuffisantes et ces derniers manquent
d'expérience. Les logiciels espions, virus et autres programmes
malveillants contribuent eux aussi à l'incertitude des utilisateurs.


La question qui se pose pour l'enseignement de l'informatique est de
savoir comment faire des erreurs le sujet d'un cours. L'objectif est que
les élèves puissent acquérir plus d'autonomie dans la résolution des
problèmes. Pour ce faire, il faut tout d'abord amener les étudiants à
prendre conscience du fait que face à un problème ou à une erreur, il
est tout à fait acceptable et même fortement profitable de l'examiner en
profondeur.


Dans *Denkfallen und Programmierfehler*, Timm Grams propose une
classification évocatrice des différents types d'erreurs commises par
les personnes [Gra90]. La classification indique également les raisons
pour lesquelles nous commettons ce genre d'erreur (voir Figure 26.1). Il
est utile de savoir que certaines erreurs (erreurs par excès de
confiance en soi, fautes de raisonnement) sont normales et peuvent
arriver quasiment à n'importe qui.


Grams distingue différents principes qui nous amènent à commettre des
erreurs, par exemple le principe dit du projecteur. Notre mémoire à
court terme possédant une capacité limitée, notre perception subit un
effet de goulot d'étranglement. La conséquence est que seule une petite
partie de l'immense volume d'informations qui nous est présenté sera
choisie de manière sélective et traitée consciemment. Les informations
qui suscitent consciemment notre intérêt dépendent du « projecteur de
notre attention ». Nous sommes réceptifs aux informations sur lesquelles
nous sommes en train de nous concentrer et ignorons inconsciemment
celles qui n'apparaissent pas dans la lumière du projecteur de notre
attention. Ce principe du projecteur est l'une des principales causes de
nos erreurs et il est intéressant dans l'enseignement d'examiner plus
précisément les causes de celles-ci. La connaissance de l'origine des
erreurs et notre capacité à les gérer présente en outre un intérêt
pédagogique certain.


	Erreurs  
	Omissions  
	Gaffes  
	Individuelle  
	Excès de confiance en soi, fautes de raisonnement  
	Don  
	Formation  
	Comportement inné  
	Comportement acquis

**Fig. 26.1.** Classification des types d'erreurs


Pour aborder le thème des erreurs dans l'enseignement, nos suggérons la
démarche suivante :


**faire accepter les erreurs**  
Les erreurs font partie de la vie
quotidienne, qu'elles soient de notre propre fait ou la conséquence des
erreurs des autres, elles font partie intégrante de l'informatique. Nous
pouvons tirer de nombreux enseignements des erreurs, pour peu que nous
les examinions de plus près et que nous ne nous contentions pas de les
faire disparaître. Les erreurs peuvent être un indicateur d'un élément
que nous n'avons pas encore bien compris. La classification des erreurs
peut aider les élèves à voir leurs propres erreurs dans un contexte plus
large. Des exemples choisis de certaines erreurs évoquées dans les
médias peuvent illustrer encore plus l'importance de ce sujet.


**faire rechercher la cause d'une erreur**  
La recherche de la cause
d'une erreur par les étudiants eux-mêmes peut s'avérer longue et
éprouvante, mais il est très important qu'ils en aient la possibilité.
L'enseignant doit ici apporter son aide en formulant des questions ou en
suggérant une procédure. Il doit toutefois intervenir le moins possible
et se contenter de rendre explicite le processus de recherche d'erreur.


**traitement des stratégies de résolution des problèmes**  
Il existe
différentes stratégies de résolution rationnelle des problèmes. Grams
mentionne par exemple les stratégies suivantes : analogie, équivalence,
projection, spécialisation, généralisation, variation, recherche en
arrière et en avant, diviser pour régner et traçabilité à un nombre fini
de cas [Gra90]. Le principe de diviser pour régner intervient très
souvent : de nombreuses causes sont éliminées grâce à des questions
habilement choisies, permettant ainsi d'isoler progressivement l'erreur
(Figure 26.2). Cette approche permet en outre d'attribuer les erreurs
aux différentes couches d'un système informatique : matériel,
périphériques, systèmes d'exploitation, pilotes ou application, machine
locale, serveur ou réseau.



	Est-il possible d'imprimer un document contenant une seule ligne et
	aucune image ?  
	Est-il possible d'imprimer depuis d'autres applications ?  
	Y a-t-il des commentaires dans le gestionnaire d'impression ?  
	L'imprimante est-elle allumée ? Contient-elle du papier ?  
	D'autres personnes peuvent-elles imprimer depuis cet ordinateur ?  
	Est-il possible d'imprimer un document contenant une seule ligne et des
	images ?  
	L'image a-t-elle un format spécial ?  
	L'image est-elle très grande ?  
	*oui*  
	*non*

**Fig. 26.2.** Isolation systématique de l'erreur


**aide à l'auto-assistance**  
Des solutions utiles et efficaces à de
nombreux problèmes peuvent être trouvées à l'aide des sources
informations disponibles localement ou sur Internet. Les élèves peuvent
être encouragés à rechercher eux-mêmes des solutions. Une recherche sur
Internet du message d'erreur précis est généralement suffisante, car
d'autres personnes ont déjà résolu le problème.


Certains outils appropriés peuvent s'avérer très utiles en cas de
problèmes de développement (sites Web, bases de données, logiciels,
etc.). La barre d'outils Web Developer pour Firefox, par exemple, permet
de visualiser la structure d'une page Web et les validateurs HTML et CSS
du W3C dénichent les erreurs de syntaxe.


**FAQ des problèmes et erreurs les plus courants**  
Les problèmes et les
erreurs récurrents sont consignés dans une FAQ pour pouvoir être
solutionnés plus rapidement à l'avenir.


Dans l'enseignement, un cours peut être consacré aux erreurs les plus
courantes. Le livre *Computer-Related* *Risks* de Peter G. Neumann
[Neu95] est une bonne référence à ce sujet. Il décrit un large
éventail de problèmes causés par des erreurs de manipulation, des
vulnérabilités dans la sécurité, des attaques malveillantes ou encore
des matériels et logiciels défectueux. De nombreuses autres pannes
informatiques ont été citées dans les médias et y sont bien
documentées : explosion d'Ariane 5 (1996), erreur de division du
processeur Pentium (1994), erreur des missiles Patriot (1991),
disparition de Mars Climate Orbiter (1998), la débâcle des bagages à
Denver (1995), la machine de radiothérapie Therac 25 (1985--1987).


**Solution :** l'enseignement de l'informatique peut inclure une
sensibilisation à l'omniprésence des erreurs. Les enseignants ne
devraient pas corriger eux-mêmes les erreurs, mais systématiquement
faire de leur gestion un sujet de cours et communiquer des stratégies et
des méthodes de recherche et de résolution des erreurs.


**Exemple 1 : effets secondaires indésirables avec les références**


Dans Word et d'autres traitements de texte, la fonction de renvoi permet
de créer des références à des images et d'autres parties du texte. Word
peut ici se référer au numéro de page d'un passage du texte ou citer
directement ce dernier. La partie gauche de la Figure 26.3 illustre le
renvoi à un titre de chapitre (surligné en gris). Ce renvoi a été créé
avec la fonction « Insertion *→* Renvoi *→* Titre » (*sous Word 2007*).



|  | **Avant le traitement**  | **Après le traitement** |
| -----------|-----------|-----------|
| **Texte** | Position du curseur avant l’insertion de « préface... »<p>Introduction Ce texte aborde les problèmes qui se posent avec les références dans Word. Attention aux fautes de raisonnement ! Les choses ne sont pas aussi simples qu’elles n’y paraissent...</p> |Préface<p>L’auteur souhaite ici remercier tout particulièrement...</p><p>Introduction</p><p>Ce texte aborde les problèmes qui se posent avec les références dans Word. Attention aux fautes de raisonnement ! Les choses ne sont pas aussi simples qu’elles n’y paraissent...</p> |
| **Renvoi** | Graphiques matriciels ou vectoriels, compression <p>avec/sans perte d’informations, etc. </p>| Comme indiqué dans Préface<p>L’auteur souhaite ici remercier tout particulièrement...</p><p>l’introduction, nous allons ici aborder les…</p> |

**Fig. 26.3.** Renvoi avant et après la révision du texte


Si l'on souhaite à présent insérer une préface avant l'introduction, on
peut être tenté de placer tout simplement le curseur au début de la
ligne du titre « Introduction », de taper le nouveau titre « Préface »,
puis d'appuyer sur Entrée et de commencer ensuite la saisie du texte
correspondant. Le renvoi à l'introduction sera alors remis
ultérieurement à jour, car le titre aura été modifié. La partie droite
de la Figure 26.3 illustre le résultat de cette révision en apparence
anodine du document : la totalité du nouveau texte saisi est à présent
considérée comme faisant partie du titre « Introduction ».


Que s'est-il passé ? Il s'agit ici d'une faute de raisonnement que la
plupart des utilisateurs serait susceptible de commettre dans une telle
situation. Word doit placer des repères au début et à la fin du texte de
référence et adapter ces repères pour que l'utilisateur puisse
éventuellement corriger le texte. Par conséquent, le texte qui sera
saisi entre ces repères doit exclusivement correspondre au renvoi
défini. Un éventuel nouveau texte doit être saisi avant l'existant et ne
doit pas simplement en être séparé par un saut de ligne.


Examinons à présent le même problème avec un langage à balises tel que
le HTML, par exemple. En HTML, le titre serait séparé par des balises :
**\<h1\>** Introduction **\</h1\>**. Grâce à cette délimitation explicite, il
devient évident qu'un nouveau titre en HTML devra être saisi sous la
forme d'un élément h1 indépendant et non pas en ajoutant du texte à un
titre existant. Cette structure des éléments n'est toutefois visible
qu'en travaillant dans le texte source HTML. Le même problème que celui
rencontré avec Word se posera dès lors que l'on utilise un éditeur
WYSIWYG.


**Exemple 2 : hypothèses implicites en programmation**


Une tâche de programmation simple illustre comment des hypothèses
implicites, combinées avec son « projecteur d'attention » propre,
peuvent induire en erreur (extrait de [Gra90]). Nous supposons ici que
l'opérateur *<* n'existe pas dans notre langage de programmation. Il
faut donc mettre en œuvre une fonction LESS(a, b: REAL): BOOLEAN,
laquelle produit précisément la valeur TRUE lorsque *a < b* est
vérifié. Les 19 développeurs interrogés ont proposé les solutions
suivantes :


1. 5 mentions de :  
[IF (b-a]{lang="en-GB"}[*>*]{lang="en-GB"}[0) THEN less:=TRUE ELSE
less:=FALSE]{lang="en-GB"}


2. 4 mentions de :  
[IF (b]{lang="en-GB"}[*>*]{lang="en-GB"}[a) THEN less:=TRUE ELSE
less:=FALSE]{lang="en-GB"}


3. 3 mentions de :  
[IF (NOT(a]{lang="en-GB"}[*>*]{lang="en-GB"}[=b)) THEN less:=TRUE ELSE
less:=FALSE]{lang="en-GB"}


4. 2 mentions de :  
[IF (a]{lang="en-GB"}[*>*]{lang="en-GB"}[b) OR (a=b) THEN less:=FALSE
ELSE less:=TRUE]{lang="en-GB"}


5. 2 mentions de :  
[IF (a]{lang="en-GB"}[*>*]{lang="en-GB"}[=b) THEN less:=FALSE ELSE
less:=TRUE]{lang="en-GB"}


6. 2 mentions de :  
[less:=NOT(a]{lang="en-GB"}[*>*]{lang="en-GB"}[=b)]{lang="en-GB"}


7. 1 mention de :  
less:=b*>*a


Toutes les solutions mentionnées sont correctes, la plus simple --
less:=b*>*a -- étant cependant la plus difficile à trouver. Il semble
qu'il existe ici pour l'essentiel deux types de fautes de raisonnement.
Les développeurs ne réussissent pas à séparer les expressions booléennes
des décisions IF-THEN-ELSE, alors que toutes les syntaxes de décision
mentionnées peuvent très facilement être transformées en expressions
relationnelles comme dans la solution n° 7. L'ordre syntaxique *a* et
*b* est en outre inchangé dans la moitié des solutions proposées, alors
que la solution la plus simple résulte justement d'une modification de
cet ordre. L'hypothèse implicite qui semble avoir été adoptée ici est
que l'ordre prédéfini est accepté sans être remis en question.


Cet exemple illustre comment de petits problèmes bien choisis peuvent
donner lieu à un cours consacré aux différents types d'erreur. Les
exemples à priori les plus insignifiants sont bien souvent les plus
révélateurs.


**Exemple 3 : petit programme, grosse erreur**


Nous allons examiner ici un exemple de programmation qui illustre
différents types d'erreurs. Le programme en Pascal suivant (extrait de
[Gra90]) doit calculer le plus grande carré *p* qui n'est pas
supérieur à un nombre *z* prédéfini (*z ≥* 1). Mathématiquement parlant,
nous recherchons *p* = 2*i*, avec :

<img alt="Fig. 26" src="Images/26_figure.png" width="200px" height="50"/>


Le programme :

	01 PROGRAM Carré;  
	02 VAR z, p: INTEGER  
	03 BEGIN  
	04 write('? z = ');  
	05 readln(z);  
	06 p := 1;  
	07 REPEAT  
	08 p := p*2;  
	09 UNTIL p*2 >= z;  
	10 writeln('! p = ',p);  
	11 END.


Ce programme contient plusieurs erreurs. Quatre d'entre elles ont été
découvertes lors de la compilation et du test du programme :

-   omission dans la ligne 2 : le point-virgule final est manquant.

-   à la ligne 5, une erreur plus personnelle qui peut être attribuée à
    une formation insuffisante : aucun contrôle de validité de la
    saisie.

-   aux lignes 7 à 9, une erreur plus personnelle qui peut être
    attribuée à une formation insuffisante : le programme échoue si l'on
    saisit *z* = 1, parce que *p* est multiplié par 2 avant de tester la
    condition d'interruption UNTIL p*2 *>*= z. La solution pourrait
    consister ici à écrire une boucle répétitive WHILE.

-   à la ligne 9, une erreur plus personnelle qui peut être considérée
    comme une maladresse : la comparaison « supérieur ou égal à » échoue
    lorsque *z* est un carré. Il faut utiliser une comparaison
    « supérieur à »._

L'erreur la plus grave est la faute de raisonnement de la ligne 9 : la
condition d'interruption UNTIL p*2 *>* z a pour effet que le programme
échoue pour la moitié des nombres entiers (INTEGER) positifs. La
condition correcte évite la multiplication en divisant l'autre côté de
l'inégalité : UNTIL p *>* z/2.
